(ns aoc.day-fifteen
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.java.io :as io])
  (:require [clojure.set :as set]))

(defn read-file
  []
  (-> (io/resource "input_15.txt")
      (slurp)
      (str/split #",")))

(defn parse
  [file-data]
  ;(reduce #(apply assoc %1 %2)
  (reduce #(assoc-in %1 (vector (first %2) :pos) (last %2))
  ;(reduce #(println %2)
  {}
  (map-indexed #(vector (keyword (str/trim %2)) (conj nil (inc %1)))
  file-data)))

(parse (read-file))


;create data structure that looks like {:number 2 :turns-said [1 2 3 4]} -- should determine next number
;keep running counter of numbers said and end when it hits target

(defn get-next-number
  [numbers-map previous-number]
  (if (= (count (get-in numbers-map (vector previous-number :pos))) 1)
    0
    (Math/abs (apply - (take 2 (get-in numbers-map (vector previous-number :pos)))))))

(defn part-1
  [numbers-map target-number]
  (loop [current-num-count (inc (count (keys numbers-map)))
         numbers-map numbers-map
         previous-number (last (keys numbers-map))]
      ;(println numbers-map)
      ;(println (get-next-number numbers-map previous-number))
      ;(println)
      (if (= current-num-count target-number)
        (get-next-number numbers-map previous-number)
        (recur (inc current-num-count) (update-in numbers-map (vector (keyword (str (get-next-number numbers-map previous-number))) :pos) conj current-num-count) (keyword (str (get-next-number numbers-map previous-number)))))))

(part-1 (parse (read-file)) 2020)
