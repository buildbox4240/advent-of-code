(ns aoc.day-three
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.java.io :as io]))

(defn read-file-into-vector
  []
  (-> (io/resource "input_3.txt")
      (slurp)
      (str/split #"\n")))

(defn build-map-structure
  [map-data]
  (-> (assoc {} :mapv map-data)
      (assoc :max-x (count (get map-data 0)))
      (assoc :max-y (count map-data))))

(defn get-map-at-coords
  [map-structure coords]
  (if (< (:x coords) (:max-x map-structure))
    (-> (:mapv map-structure)
        (get (:y coords))
        (.charAt (:x coords)))
    (-> (:mapv map-structure)
        (get (:y coords))
        (.charAt (mod (:x coords) (:max-x map-structure))))))

(defn generate-next-coordinates
  [coordinates x-increment y-increment]
    (lazy-seq
    (cons coordinates (generate-next-coordinates (update (update coordinates :x + x-increment) :y + y-increment) x-increment y-increment))))

(defn get-number-of-trees
  [map-structure x-increment y-increment]
  (reduce
    (fn [no-of-trees current-coords]
      (if (= (get-map-at-coords map-structure current-coords) \#)
        (inc no-of-trees)
        no-of-trees))
    0
    (take (int (Math/floor (/ (:max-y map-structure) y-increment))) (generate-next-coordinates {:x 0 :y 0} x-increment y-increment))))

(defn -main
  [& args]
  (println (get-number-of-trees (build-map-structure (read-file-into-vector)) 3 1))
  (println (-> (get-number-of-trees (build-map-structure (read-file-into-vector)) 1 1)
               (* (get-number-of-trees (build-map-structure (read-file-into-vector)) 3 1))
               (* (get-number-of-trees (build-map-structure (read-file-into-vector)) 5 1))
               (* (get-number-of-trees (build-map-structure (read-file-into-vector)) 7 1))
               (* (get-number-of-trees (build-map-structure (read-file-into-vector)) 1 2)))))
