(ns aoc.day-two
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.java.io :as io]))

(defn read-file-into-seq
  []
  (str/split (slurp (io/resource "input_2.txt")) #"\n"))

(defn remove-last-character
  [s]
  (.substring s 0 (- (count s) 1)))

(defn count-character-occurences
  [s c]
  (reduce
    (fn [char-count current-char]
      ;not entirely sure how to do this in clojure, using (first) to convert str to char
      (if (= (first c) current-char)
        (inc char-count)
        char-count))
    0
    (seq s)))

(defn get-password-rules-map
  "Returns a map like {:min 2 :max 5 :character \"l\" :password \"gllsl\""
  []
  (map
    (fn [password-line]
      (let [split-line (str/split password-line #" ")
           split-range (str/split (nth split-line 0) #"-")]
        {
           :min (Integer. (nth split-range 0))
           :max (Integer. (nth split-range 1))
           :character (remove-last-character(nth split-line 1))
           :password (nth split-line 2)
        }))
    (read-file-into-seq)))

(defn get-number-of-valid-passwords
  []
  (reduce
    (fn [no-of-valid-passwords password-map]
      (if (and
          (>= (count-character-occurences (:password password-map) (:character password-map)) (:min password-map))
          (<= (count-character-occurences (:password password-map) (:character password-map)) (:max password-map)))
        (inc no-of-valid-passwords)
        no-of-valid-passwords))
    0
    (get-password-rules-map)))

(defmacro simple-xor
  "macro that evaluates xor for 2 expressions"
  [expr1 expr2]
  `(or (and ~expr1 (not ~expr2)) (and (not ~expr1) ~expr2)))

(defn get-number-of-valid-passwords-part-two
  []
  (reduce
    (fn [no-of-valid-passwords password-map]
      (if (simple-xor
          ;dec to account for no 0 index
          ;first to get string as character
          (= (.charAt (:password password-map) (dec (:min password-map))) (first (:character password-map)))
          (=  (.charAt (:password password-map) (dec (:max password-map))) (first (:character password-map))))
        (inc no-of-valid-passwords)
        no-of-valid-passwords))
    0
    (get-password-rules-map)))

(defn -main
  [& args]
  (println (get-number-of-valid-passwords))
  (println (get-number-of-valid-passwords-part-two)))
