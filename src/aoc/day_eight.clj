(ns aoc.day-eight
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.java.io :as io])
  (:require [clojure.set :as set]))

(defn read-file-into-vector
  []
  (-> (io/resource "input_8.txt")
      (slurp)
      (str/split #"\n")))

(defn parse-int-from-instruction
  [instruction]
  (Integer. (second (str/split instruction #" "))))

;part-1
(defn run-program
  [instructions]
  (loop [instruction-stack instructions
        pointer 0
        acc 0]
    (let [current-instruction (get instruction-stack pointer)]
      (cond
        (= pointer (count instruction-stack))
          acc
        (empty? current-instruction)
          acc
        (str/includes? current-instruction "nop")
          (recur (assoc instruction-stack pointer "") (inc pointer) acc)
        (str/includes? current-instruction "jmp")
          (recur (assoc instruction-stack pointer "") (+ pointer (parse-int-from-instruction current-instruction)) acc)
        (str/includes? current-instruction "acc")
          (recur (assoc instruction-stack pointer "") (inc pointer)  (+ acc (parse-int-from-instruction current-instruction)))))))

(run-program (read-file-into-vector))

;part2
(defn find-infinite-loop
  [instructions]
  (loop [instruction-stack instructions
        pointer 0]
    (let [current-instruction (get instruction-stack pointer)]
      (cond
        (= pointer (count instruction-stack))
          :not-infinite-loop
        (empty? current-instruction)
          :infinite-loop
        (str/includes? current-instruction "nop")
          (recur (assoc instruction-stack pointer "") (inc pointer))
        (str/includes? current-instruction "jmp")
          (recur (assoc instruction-stack pointer "") (+ pointer (parse-int-from-instruction current-instruction)))
        (str/includes? current-instruction "acc")
          (recur (assoc instruction-stack pointer "") (inc pointer))))))

;swap nops and jmps 1 by 1 until a valid program is found. There is probably a cleaner way to do this
(defn get-fixed-program
  [instructions]
  (filter #(not (nil? %))
    (for [i (range (count instructions))]
      (cond
        (str/includes? (get instructions i) "nop")
          (let [new-instructions (assoc instructions i (str/replace (get instructions i) "nop" "jmp"))]
            (if (= :not-infinite-loop (find-infinite-loop new-instructions))
              new-instructions))
        (str/includes? (get instructions i) "jmp")
          (let [new-instructions (assoc instructions i (str/replace (get instructions i) "jmp" "nop"))]
            (if (= :not-infinite-loop (find-infinite-loop new-instructions))
              new-instructions))))))

(run-program (first (get-fixed-program (read-file-into-vector))))
