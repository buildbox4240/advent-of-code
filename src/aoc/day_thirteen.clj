(ns aoc.day-thirteen
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.java.io :as io])
  (:require [clojure.set :as set]))

(defn read-file
  []
  (-> (io/resource "input_13.txt")
      (slurp)
      (str/split #"\n")))

(defn parse
  []
  (let [file (read-file)]
    (vector (Integer. (first file)) (map #(Integer. %) (filter #(not= "x" %) (str/split (second file) #","))))))

(parse)

(defn part-1
  [[arrive-time shuttles]]
  (loop [current-time arrive-time
        valid-shuttle-times (filter #(= (mod current-time %) 0) shuttles)]
    (if (not (empty? valid-shuttle-times))
        (* (- current-time arrive-time) (first valid-shuttle-times))
        (recur (inc current-time) (filter #(= (mod (inc current-time) %) 0) shuttles)))))

(empty? (filter #(= (mod 944 %) 0) (second (parse))))

(part-1 (parse))
