(ns aoc.day-ten
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.java.io :as io])
  (:require [clojure.set :as set]))

(defn read-file
  []
  (map #(Integer. %)
    (-> (io/resource "input_10.txt")
        (slurp)
        (str/split #"\n"))))

(count (read-file))

;part 1
(def numbers
  (->>
    (read-file)
    (cons 0)))

(def diff-freq
  (frequencies (map #(- (second %) (first %)) (partition 2 1 (sort numbers)))))

;answer
; added an extra 3-count for the connection from the last adapter to the phone which has a diff of 3
(* (get diff-freq 1) (inc (get diff-freq 3)))

(def numbers-part-2
  (sort (conj numbers (+ (reduce max numbers) 3))))

;part 2
(defn count-adapter-arrangements
  [start end adapters]
  (cond
    (>= start end)
      0
    (nil? (some #(= start %) adapters))
      0
    (<= 3 (- end start) 3)
      (+ 1 (count-adapter-arrangements (+ start 1) end adapters) (count-adapter-arrangements (+ start 2) end adapters) (count-adapter-arrangements (+ start 3) end adapters))
    :else
      (+ (count-adapter-arrangements (+ start 1) end adapters) (count-adapter-arrangements (+ start 2) end adapters) (count-adapter-arrangements (+ start 3) end adapters))))

(def memoized-count (memoize count-adapter-arrangements))

(memoized-count 0 (last numbers-part-2) numbers-part-2)
