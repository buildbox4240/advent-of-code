(ns aoc.day-twelve
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.java.io :as io])
  (:require [clojure.set :as set]))

(defn read-file
  []
  (-> (io/resource "input_12.txt")
      (slurp)
      (str/split #"\n")))

(def parsed-instructions
  (map #(vector (second (first %)) (Integer. (last (first %)))) (map #(re-seq #"(?:([NSEWLRF])(\d+))" %) (read-file))))
;(map #(println %) (map #(re-seq #"(?:([NSEWLRF])(\d+))" %) (read-file)))

;ship direction
;changed by r/l
;F moves it forward in direction it is facing -- would probably be easier to translate this into an E10, W10, etc and parse it all in one place
;expressed in bearing, L is - and R is +

;ship movement
;moving s should move it n-s to the s
;moving n should move it s-n to the n
;moving e should move it w-e to the e
;moving w should move it e-w to the w
;position shown as {e: 10, n: 5}

(def opposite-directions
  {:north :south :south :north :east :west :west :east})

(def bearing-direction
  {0 :north 90 :east 180 :south 270 :west})

(def ship-direction
  90)

(def ship-position
  {:north 0 :south 0 :east 0 :west 0})

(def ship
  {:direction ship-direction :position ship-position})

(defn turn-ship
  [ship magnitude]
  (update ship :direction #(mod (+ magnitude %) 360)))

(defn move-in-direction
  [ship direction magnitude]
  (let [opposite-direction (get opposite-directions direction)
        opposite-magnitude (get (get ship :position) opposite-direction)
        current-magnitude (get (get ship :position) direction)
        magnitude-diff (- (+ current-magnitude magnitude) opposite-magnitude)]
    (if (< magnitude-diff 0)
      (-> (assoc-in ship [:position direction] 0)
          (assoc-in [:position opposite-direction] (Math/abs magnitude-diff)))
      (-> (assoc-in ship [:position opposite-direction] 0)
          (assoc-in [:position direction] magnitude-diff)))))

(defn move-ship
  [ship direction magnitude]
  (cond
    (= direction "F")
      (move-in-direction ship (get bearing-direction (get ship :direction)) magnitude)
    (= direction "N")
      (move-in-direction ship :north magnitude)
    (= direction "S")
      (move-in-direction ship :south magnitude)
    (= direction "E")
      (move-in-direction ship :east magnitude)
    (= direction "W")
      (move-in-direction ship :west magnitude)))

(defn perform-instruction
  [ship [direction magnitude]]
  (cond
    (or (= direction "F") (= direction "N") (= direction "S") (= direction "E") (= direction "W"))
      (move-ship ship direction magnitude)
    (= direction "R")
      (turn-ship ship magnitude)
    (= direction "L")
      (turn-ship ship (- 0 magnitude))))

(defn part-1
  [parsed-instructions ship]
  (loop [[current & remaining-instructions] parsed-instructions
        ship ship]
    (if (nil? current)
      (do (println ship) ship)
      (recur remaining-instructions (perform-instruction ship current)))))

(defn manhattan-distance
  [ship-position]
  (+ (get ship-position :north) (get ship-position :south) (get ship-position :east) (get ship-position :west)))

(manhattan-distance (get (part-1 parsed-instructions ship) :position))
