(ns aoc.day-five
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.java.io :as io]))

(defn read-file-into-vector
  []
  (-> (io/resource "input_5.txt")
      (slurp)
      (str/split #"\n")))

(def sample-input "FBFBBFFRLR")

(defn determine-seat
  [seat-string]
  (let [row-number (-> (subs seat-string 0 (- (count seat-string) 3))
                       (str/split #"")
                       (binary-space-partitioning 0 127))
        seat-number (-> (subs seat-string (- (count seat-string) 3))
                        (str/split #"")
                        (binary-space-partitioning 0 7))]
        {:row row-number :seat seat-number}))

(defn binary-space-partitioning
  ([final-value]
  final-value)
  ([input current-min current-max]
  (let [[current-char & remaining] input
        diff (Math/ceil (/ (- current-max current-min) 2))]
    (cond
      (nil? current-char)
        (binary-space-partitioning current-max)
      (or (= current-char "F" ) (= current-char "L"))
        (binary-space-partitioning remaining current-min (- current-max diff))
      (or (= current-char "B") (= current-char "R"))
        (binary-space-partitioning remaining (+ diff current-min) current-max)))))

(defn part-1
  []
  (reduce
    (fn [current-max current-seat-string]
      (let [seat (determine-seat current-seat-string)
            seat-id (+ (* (:row seat) 8) (:seat seat))]
       (if (> seat-id current-max)
        seat-id
        current-max)))
    0
    (read-file-into-vector)))

(defn build-sorted-seat-id-seq;
  []
  (sort (map
    (fn [current-seat-string]
      (let [seat (determine-seat current-seat-string)]
        (+ (* (:row seat) 8) (:seat seat))))
    (read-file-into-vector))))

(defn find-missing-seat
  [sorted-coll]
  (->> (partition 2 1 sorted-coll)
       (filter #(> (- (second %) (first %)) 1))))



(defn -main
  [& args]
  (println (part-1))
  (println (find-missing-seat (build-sorted-seat-id-seq))))
