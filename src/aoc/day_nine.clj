(ns aoc.day-nine
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.java.io :as io])
  (:require [clojure.set :as set]))

(defn read-file
  []
  (map #(Integer. %)
    (-> (io/resource "input_9.txt")
        (slurp)
        (str/split #"\n"))))

;part 1
(defn find-first-invalid-number
  ([preamble numbers]
    (find-first-invalid-number preamble numbers 0 (dec preamble) (dec preamble) preamble))
  ([preamble numbers low-index high-index floating-index current-index]
    (loop [low-idx low-index
           high-idx high-index
           floating-idx floating-index
           current-idx current-index]
      (let [current-number (nth numbers current-idx)
            sum (+ (nth numbers low-idx) (nth numbers floating-idx))]
        (cond
          (= low-idx high-idx)
            ;no sum found
            ;return number
            current-number
          (= low-idx floating-idx)
            ;didn't find a sum with this low-idx
            ;move on to the next low-idx
            (recur (inc low-idx) high-idx high-idx current-idx)
          (= current-number sum)
            ;found valid sum
            ;move on to the next number
            (recur (- (inc current-idx) preamble) (inc high-idx) (inc high-idx) (inc current-idx))
          :else
            ;move the floating idx
            (recur low-idx high-idx (dec floating-idx) current-idx))))))

;(find-first-invalid-number 25 (read-file))

;part 2
(defn get-index-range
  [numbers target]
  (loop [start 0
         end 1
         sum 1]
      (cond
        (= sum target)
          ;return answer
          {:start start :end end}
        (< sum target)
          ;increase window
          (recur start (inc end) (+ sum (nth numbers (inc end))))
        (> sum target)
          ;start new window
          (recur (inc start) (+ start 2) (+ (nth numbers (inc start)) (nth numbers (+ start 2)))))))

(defn add-smallest-largest-in-range
  [start end numbers]
  (let [sorted-nums (sort (for [i (range start (inc end))]
                      (nth numbers i)))]
    (+ (nth sorted-nums 0) (nth sorted-nums (dec (count sorted-nums))))))

(defn part-two
  []
  (let [numbers (read-file)
        rng (get-index-range numbers 50047984)]
    (add-smallest-largest-in-range (:start rng) (:end rng) numbers)))

(part-two)
;(get-index-range (read-file) 50047984)
