(ns aoc.day-eleven
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.java.io :as io])
  (:require [clojure.set :as set]))

(defn read-file
  []
  (-> (io/resource "input_11.txt")
      (slurp)
      (str/split #"\n")))

(read-file)

(defn replace-at [s idx replacement]
  (str (subs s 0 idx) replacement (subs s (inc idx))))

(defn seat-open?
  [seat]
  (or (= \. seat) (= \L seat)))

(defn get-seat-at-coords
  [{x :x y :y} seat-map]
  (-> (nth seat-map y)
      (.charAt x)))

(defn occupy-seat
  [{x :x y :y} seat-map]
  (->> (-> (nth seat-map y)
           (replace-at x "#"))
    (assoc seat-map y)))

(defn empty-seat
  [{x :x y :y} seat-map]
  (->> (-> (nth seat-map y)
           (replace-at x "L"))
    (assoc seat-map y)))

(def seats
  (let [seats (read-file)]
    {:seats seats :max-x (dec (count (nth seats 0))) :max-y (dec (count seats))}))

(defn get-adjacent-seat-coords
  [{x :x y :y} {seats :seats max-x :max-x max-y :max-y}]
  (filter #(not (or (< (:x %) 0) (< (:y %) 0) (< max-x (:x %)) (< max-y (:y %))))
  (vector
    {:x (inc x) :y y} {:x x :y (inc y)} {:x x :y (dec y)} {:x (dec x) :y y}
    {:x (inc x) :y (inc y)} {:x (dec x) :y (dec y)} {:x (inc x) :y (dec y)} {:x (dec x) :y (inc y)})))

(defn get-adjacent-seats
  [coords seats]
  (map #(get-seat-at-coords % (:seats seats)) (get-adjacent-seat-coords coords seats)))

(defn get-next-coords
  [{x :x y :y} {seats :seats max-x :max-x max-y :max-y}]
  (cond
    (and (= max-x x) (= max-y y))
    nil
    (= max-x x)
    {:x 0 :y (inc y)}
    :else
    {:x (inc x) :y y}))

(get-adjacent-seats {:x 0 :y 0} seats)
(get-next-coords {:x 9 :y 0} seats)

(occupy-seat {:x 0 :y 0} (:seats seats))

(defn find-changes
  [seats-map]
  (loop [coords {:x 0 :y 0}
         changes 0
         seats seats-map
         new-seats seats-map]
    ;(println (:seats new-seats))
    (cond
      (= nil coords)
        {:changes changes :new-seats new-seats}
      ;if floor, continue
      (= (get-seat-at-coords coords (:seats seats)) \.)
        (recur (get-next-coords coords seats) changes seats new-seats)
      ;if seat open and all adjacents open, fill it
      (and (nil? (get (frequencies (get-adjacent-seats coords seats)) \#)) (= (get-seat-at-coords coords (:seats seats)) \L))
        (recur (get-next-coords coords seats) (inc changes) seats {:seats (occupy-seat coords (:seats new-seats)) :max-x (:max-x new-seats) :max-y (:max-y new-seats)})
      ;if seat filled and 4 other seats next to it filled, empty it
      (and (not (nil? (get (frequencies (get-adjacent-seats coords seats)) \#))) (>= (get (frequencies (get-adjacent-seats coords seats)) \#) 4) (= (get-seat-at-coords coords (:seats seats)) \#))
        (recur (get-next-coords coords seats) (inc changes) seats {:seats (empty-seat coords (:seats new-seats)) :max-x (:max-x new-seats) :max-y (:max-y new-seats)})
      :else
        (recur (get-next-coords coords seats) changes seats new-seats))))

;part 1
(defn part-1
[]
  (loop [seats seats]
    (let [results (find-changes seats)]
      (if (= (:changes results) 0)
        (reduce
          (fn [acc row]
            (if (nil? (get (frequencies row) \#))
              acc
              (+ acc (get (frequencies row) \#))))
        0 (:seats (:new-seats results)))
        (recur (:new-seats results))))))
