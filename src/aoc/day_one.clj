(ns aoc.day-one
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.java.io :as io]))

(defn read-file-into-seq
 []
 (map
  (fn [current-string]
    (Integer. current-string))
  (str/split (slurp (io/resource "input_1.txt")) #"\n")))

(defn find-sum
  [sorted-coll target first-index last-index]
  (cond
    (= (+ (nth sorted-coll first-index) (nth sorted-coll last-index)) target)
      (* (nth sorted-coll first-index) (nth sorted-coll last-index))
    (> (+ (nth sorted-coll first-index) (nth sorted-coll last-index)) target)
      (find-sum sorted-coll target first-index (- last-index 1))
    (< (+ (nth sorted-coll first-index) (nth sorted-coll last-index)) target)
      (find-sum sorted-coll target (+ first-index 1) last-index)))

(defn find-sum-part-two
  [sorted-coll target first-index mid-index last-index]
  (cond
    (or (> mid-index last-index) (= mid-index last-index))
      (find-sum-part-two sorted-coll target (+ first-index 1) (+ first-index 2) (- (count sorted-coll) 1))
    (= (+ (nth sorted-coll first-index) (nth sorted-coll mid-index) (nth sorted-coll last-index)) target)
      (* (nth sorted-coll first-index) (nth sorted-coll mid-index) (nth sorted-coll last-index))
    (> (+ (nth sorted-coll first-index) (nth sorted-coll mid-index) (nth sorted-coll last-index)) target)
      (find-sum-part-two sorted-coll target first-index mid-index (- last-index 1))
    (< (+ (nth sorted-coll first-index) (nth sorted-coll mid-index) (nth sorted-coll last-index)) target)
      (find-sum-part-two sorted-coll target first-index (+ mid-index 1) last-index)))

(defn -main
  [& args]
  (let [sorted-coll (sort (read-file-into-seq))]
    (println (find-sum sorted-coll 2020 0 (- (count sorted-coll) 1)))
    (println (find-sum-part-two sorted-coll 2020 0 1 (- (count sorted-coll) 1)))))
