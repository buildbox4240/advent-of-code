(ns aoc.day-six
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.java.io :as io])
  (:require [clojure.set :as set]))

(defn read-file-into-vector
  []
  (-> (io/resource "input_6.txt")
      (slurp)
      (str/split #"\n\n")))

(def groups
  (map #(str/replace % #"\n" "") (read-file-into-vector)))

(defn count-uniques-for-groups
  []
  (reduce +
    (map #(count (set %)) groups)))

(defn count-same-for-groups
  []
  (reduce (fn [final-count current-group]
    (+ final-count (count (apply set/intersection (map set (str/split-lines current-group))))))
    0
    (read-file-into-vector)))

(defn -main
  [& args]
  (println "day 6!!!")
  (println (count-uniques-for-groups)))
