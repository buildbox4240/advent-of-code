(ns aoc.day-fourteen
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.java.io :as io])
  (:require [clojure.set :as set]))

(defn read-file
  []
  (-> (io/resource "input_14.txt")
      (slurp)
      (str/split #"\n")))

(defn parse-input
  []
  (map (fn [line]
        (if (= (first line) "mask ")
          [:mask (str/trim (last line))]
          [:instr (Integer. (re-find #"\d+" (str/trim (first line)))) (Integer. (str/trim (last line)))]))
    (map #(str/split % #"=")
    (read-file))))

(parse-input)

(defn convert-mask
  [mask bit]
  (->> (str/escape mask {\X bit})
       (str "2r")
       (read-string)))

(convert-mask (second (first (parse-input))) 1)

(defn apply-mask [number zero-mask one-mask]
  (bit-and (bit-or number zero-mask) one-mask))

(defn run-instructions
  []
  (loop [mask nil
         memory {}
         [current & remaining] (parse-input)]
    (cond
      (= nil current)
        memory
      (= (first current) :mask)
        (recur (second current) memory remaining)
      (= (first current) :instr)
        (recur mask (assoc memory (second current) (apply-mask (last current) (convert-mask mask 0) (convert-mask mask 1))) remaining))))

(defn part-1
  []
  (->> (seq (run-instructions))
       (reduce (fn [acc [k v current]] (+ acc v)) 0)))

(part-1)
