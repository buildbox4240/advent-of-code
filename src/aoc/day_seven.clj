;got really messy here overall, lack of clojure/functional programming experience and time constraints backed me into a wall in a few places
(ns aoc.day-seven
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.java.io :as io])
  (:require [clojure.set :as set]))

(defn read-file-into-vector
  []
  (-> (io/resource "input_7.txt")
      (slurp)
      (str/split #"\n")))

(defn build-map
  [rule-line]
  (let [[main-bag & contains] (map #(str/trim %) (str/split rule-line #"contain|,"))]
     (assoc-in {} [(str/replace main-bag #" bags" "") :contains]
       (map (fn [current-contains]
              (if (not (str/includes? contains "no other bags"))
              ;pretty messy parse function, need better regex skills!!
              {:name (str/trim (str/replace (re-find #"[^\d]+?bag" current-contains) #" bag" ""))  :qty (Integer. (re-find #"\d" current-contains))})) contains))))

(build-map (first (read-file-into-vector)))
(build-map "bright fuschia bags contain no other bags")

(def bags
  (map (fn [row] (build-map row)) (read-file-into-vector)))

(def bags-map
  (into {}
  (map (fn [row] (build-map row)) (read-file-into-vector))))

(defn get-bag-parents
  [bag-list target]
  (map #(first (keys %))
    (filter
      (fn [bag]
        (some #(= target (:name %)) (get-in bag [(first (keys bag)) :contains])))
      bag-list)))

(get-bag-parents bags "shiny gold")

;part1
(defn get-all-bag-parents
  ([bag-list target]
    (get-all-bag-parents bag-list [target] #{}))
  ([bag-list targets bag-parents]
    (if (empty? targets)
      bag-parents
      (let [new-targets (set (reduce #(into (get-bag-parents bag-list %2) %1) [] targets))]
        (get-all-bag-parents bag-list new-targets (into bag-parents new-targets))))))

(count (get-all-bag-parents bags "shiny gold"))

;part2
(defn get-child-count
  [graph color]
  (let [children (get-in graph [color :contains])]
    (if(seq children)
      (reduce
        (fn [color-count current-bag]
          (println current-bag)
          (if (not (nil? current-bag))
            (+ color-count (* (:qty current-bag) (get-child-count graph (:name current-bag))))
            1))
        1
        children)
       1)))

(dec (get-child-count bags-map "shiny gold"))

(defn -main
  [& args]
  (println "day 7!!!"))
