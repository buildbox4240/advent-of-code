; The data parsing functions are pretty bad here, tbh I found this one to be a pretty
; tedious challenge and just wanted to get through it
(ns aoc.day-four
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.java.io :as io]))

; returns ["pid:xxx" "ecl:xxx"]
(defn parse
  []
  (map
    (fn [current-passport]
      (str/split (get current-passport 0) #" "))
    (map
      (fn [current-line]
        (-> (str/replace current-line #"\n" " ")
            (str/split #"s:s")))
      (-> (io/resource "input_4.txt")
          (slurp)
          (str/split #"\n\n")))))

(defn build-all-passports-vector
  []
  (reduce
    (fn [all-passports current-passport]
      (cons
        (reduce
          (fn [passport-map current-passport]
            (let [[k v] (str/split current-passport #":")]
              (into passport-map (vector (vector (keyword k) v)))))
            {}
            current-passport)
        all-passports))
   []
   (parse)))

(defn part-1-validation
  [passport]
  (and
    (contains? passport :ecl)
    (contains? passport :eyr)
    (contains? passport :hcl)
    (contains? passport :byr)
    (contains? passport :iyr)
    (contains? passport :pid)
    (contains? passport :hgt)))

(defn verify-height
  [height]
  (cond
    (str/ends-with? height "in")
      (<= 59 (Integer. (subs height 0 (- (count height) 2))) 76)
    (str/ends-with? height "cm")
      (<= 150 (Integer. (subs height 0 (- (count height) 2))) 193)
    :else false))

(defn part-2-validation
  [passport]
  (and
    (.contains ["amb" "blu" "brn" "gry" "grn" "hzl" "oth"] (:ecl passport))
    (not (nil? (passport :iyr)))
    (<= 2010 (Integer. (:iyr passport)) 2020)
    (not (nil? (passport :eyr)))
    (<= 2020 (Integer. (:eyr passport)) 2030)
    (not (nil? (passport :byr)))
    (<= 1920 (Integer. (:byr passport)) 2002)
    (not (nil? (passport :hgt)))
    (verify-height (:hgt passport))
    (not (nil? (passport :hcl)))
    (re-matches #"#[0-9a-f]{6}" (:hcl passport))
    (not (nil? (passport :pid)))
    (re-matches #"\d{9}" (:pid passport))))

(defn part-1-count
  []
  (reduce
    (fn [no-of-valid-passports current-passport]
      (if (part-1-validation current-passport)
        (inc no-of-valid-passports)
        no-of-valid-passports))
    0
    (build-all-passports-vector)))

(defn part-2-count
  []
  (reduce
    (fn [no-of-valid-passports current-passport]
      (if (part-2-validation current-passport)
        (inc no-of-valid-passports)
        no-of-valid-passports))
    0
    (build-all-passports-vector)))

(defn -main
  [& args]
  (println (part-1-count))
  (println (part-2-count)))
